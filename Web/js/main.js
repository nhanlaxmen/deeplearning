$(document).ready(function() {
	
	$(".button--custom").click(function() {
		window.open("3dchart.html", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
	})

    var $score = $(".score-slider");
    $score.slider({
        slide: function(event, ui) {
            /* Fix handler to be inside of slider borders */
            var $Handle_score_1 = $(this).find('.ui-slider-handle');
            $Handle_score_1.css('margin-left', -1 * $Handle_score_1.width() * ($(this).slider('value') / $(this).slider('option', 'max')));
        },
        change: function(event, ui) {
            /* Fix handler to be inside of slider borders */
            var $Handle_score_1 = $(this).find('.ui-slider-handle');
            $Handle_score_1.css('margin-left', -1 * $Handle_score_1.width() * ($(this).slider('value') / $(this).slider('option', 'max')));
        }
    });
    /* Fix handler to be inside of slider borders */
    $score.each(function() {
        var $Handle_score_1 = $(this).find('.ui-slider-handle');
        $Handle_score_1.css('margin-left', -1 * $Handle_score_1.width() * ($(this).slider('value') / $(this).slider('option', 'max')));
    });


    var $score_01 = $("#score-1");
    $score_01.slider({
        range: "min",
        value: 4,
        min: 1,
        max: 20,
        step: 1
    });
    $score_01.on( "slide", function( event, ui ) {
        $( "#txt-score-1").val(ui.value.toFixed(0));
        setting_number_layers();
    } );

    function getRealValue(sliderValue) {
        if(sliderValue<=20000){
            return(sliderValue/20000*9)+1
        } else if(sliderValue>20000 && sliderValue<=40000){
            return (sliderValue-20000)/2000*9 + 10;
        } else if(sliderValue>40000 && sliderValue<=60000){
            return (sliderValue-40000)/200*9 + 100;
        }else if(sliderValue>60000 && sliderValue<=80000){
            return (sliderValue-60000)/20*9 + 1000;
        }else if(sliderValue>80000 && sliderValue<=100000){
            return (sliderValue-80000)/2*9 + 10000;
        }
            return 0;
    }
    function getFakeValue(realValue) {
        if(realValue<=10){
            return((realValue-1)/9*20000)
        } else if(realValue>10 && realValue<=100){
            return ((realValue - 10)/9*2000)+20000;
        } else if(realValue>100 && realValue<=1000){
            return ((realValue - 100)/9*200)+40000;
        }else if(realValue>1000 && realValue<=10000){
            return ((realValue - 1000)/9*20)+60000;
        }else if(realValue>10000 && realValue<=100000){
            return ((realValue - 10000)/9*2)+80000;
        }
        return 0;
    }
    $(".slider-neurons").slider({
        range: "min",
        value: 0,
        min: 1,
        max: 100000,
        step: 1
    });
    var ticks  = "<div class='number-tick' id='number2'>|</div>" +
      "<div class='number-tick' id='number3'>|</div>" +
      "<div class='number-tick' id='number4'>|</div><" +
      "div class='number-tick' id='number5'>|</div>";
    $("#hyper-slider-number-neurous, #hyper-slider-neurons-output, #hyper-slider-iteration, #hyper-slider-validation").append(ticks);
    $("#slider-neurons, #slider-neurons-output, #slider-neurons-optimizer").append(ticks);
    $(".slider-dropout").slider({
        range: "min",
        value: 0,
        min: 0.0,
        max: 1.0,
        step: 0.1
    });


    var $score_neurons = $("#slider-neurons");
    $score_neurons.on( "slide", function( event, ui ) {
        $( "#slider-neurons-txt").val(Math.round(getRealValue(ui.value.toFixed(0))));
        setting_cube();
        setting_height_line();
    } );

    $score_neurons.click(function() {
        setting_height_line();
    })

    var $score_dropout = $("#slider-dropout");
    $score_dropout.on( "slide", function( event, ui ) {
        $( "#slider-dropout-txt").text(ui.value.toFixed(1));
    } );

    var $score_neurons_output = $("#slider-neurons-output");
    $score_neurons_output.on( "slide", function( event, ui ) {
        $( "#slider-neurons-output-txt").text(Math.round(getRealValue(ui.value.toFixed(0))));
    } );

    var $score_dropout_output = $("#slider-dropout-output");
    $score_dropout_output.on( "slide", function( event, ui ) {
        $( "#slider-dropout-output-txt").text(ui.value.toFixed(1));
    } );

    var $score_neurons_optimizer = $("#slider-neurons-optimizer");
    $score_neurons_optimizer.on( "slide", function( event, ui ) {
        $( "#slider-neurons-optimizer-txt").text(Math.round(getRealValue(ui.value.toFixed(0))));
    } );



    var $range_slider = $(".range-slider");
    $range_slider.slider({
        range: true,
        slide: function(event, ui) {
            var value1 =  $(this).slider("values", 0);
            var value2 = $(this).slider("values", 1);

            var first = $(this).find(".ui-slider-handle:first");
            first.css('margin-left', -1 * 12 * (value1 / $(this).slider('option', 'max')));

            var last = $(this).find(".ui-slider-handle:last");
            last.css('margin-left', -1 * 12 * (value2 / $(this).slider('option', 'max')));
        }
    });
    /* Fix handler to be inside of slider borders */



    $(".range-slider-hyper-layer").slider({
        min: 1,
        max: 20,
        step: 1,
        values: [0, 20],
        start: function (event, ui) {
            $(this).slider().find(".ui-slider-range").addClass("update-range");
        }
    });

    $(".range-slider-hyper-neurous").slider({
        min: 1,
        max: 100000,
        step: 1,
        values: [0, 100000],
        start: function (event, ui) {
            $(this).slider().find(".ui-slider-range").addClass("update-range");
        }
    });

    $(".range-slider-hyper-dropout").slider({
        min: 0.0,
        max: 1.0,
        step: 0.1,
        values: [0, 1.0],
        start: function (event, ui) {
            $(this).slider().find(".ui-slider-range").addClass("update-range");
        }
    });

    $("#hyper-slider-number-layer").on( "slide", function( event, ui ) {
        $( "#hyper-slider-number-layer-txt-01").text(ui.values[0].toFixed(0));
        $( "#hyper-slider-number-layer-txt-02").text(ui.values[1].toFixed(0));
    } );

    $("#hyper-slider-number-neurous").on( "slide", function( event, ui ) {
        $( "#hyper-slider-number-neurous-txt-01").text(Math.round(getRealValue(ui.values[0].toFixed(0))));
        $( "#hyper-slider-number-neurous-txt-02").text(Math.round(getRealValue(ui.values[1].toFixed(0))));
    } );

    $("#hyper-slider-input-dropout").on( "slide", function( event, ui ) {
        $( "#hyper-slider-input-dropout-txt-01").text(ui.values[0].toFixed(1));
        $( "#hyper-slider-input-dropout-txt-02").text(ui.values[1].toFixed(1));
    } );

    $("#hyper-slider-inner-dropout").on( "slide", function( event, ui ) {
        $( "#hyper-slider-inner-dropout-txt-01").text(ui.values[0].toFixed(1));
        $( "#hyper-slider-inner-dropout-txt-02").text(ui.values[1].toFixed(1));
    } );

    $("#hyper-slider-neurons-output").on( "slide", function( event, ui ) {
        $( "#hyper-slider-neurons-output-txt").text(Math.round(getRealValue(ui.value.toFixed(0))));
    } );

    $("#hyper-slider-iteration").on( "slide", function( event, ui ) {
        $( "#hyper-slider-iteration-txt-01").text(Math.round(getRealValue(ui.values[0].toFixed(0))));
        $( "#hyper-slider-iteration-txt-02").text(Math.round(getRealValue(ui.values[1].toFixed(0))));
    } );

    $("#hyper-slider-validation").on( "slide", function( event, ui ) {
        $( "#hyper-slider-validation-txt").text(Math.round(getRealValue(ui.value.toFixed(0))));
    } );

    $range_slider.each(function() {
        var value1 = $(this).slider("values", 0);
        var value2 = $(this).slider("values", 1);

        var first = $(this).find(".ui-slider-handle:first");
        first.css('margin-left', -1 * 12 * (value1 / $(this).slider('option', 'max')));

        var last = $(this).find(".ui-slider-handle:last");
        last.css('margin-left', -1 * 12 * (value2 / $(this).slider('option', 'max')));
    });

    var $select_type = $(".select__type");
    var $select_mode = $(".select__mode");
    
    $select_type.change(function() {
        $(".viewport > img").attr("src","img/"+$select_mode.val()+"-"+$(this).val()+".png");
        if($select_mode.val() == "normal") {
            $(".viewport-normal-type-a").addClass("active");
            if($(this).val() == "type-b") {
                $(".viewport-normal-type-a").addClass("normal-type-b");
            } else {
                $(".viewport-normal-type-a").removeClass("normal-type-b");
            }
        } else {
            $(".viewport-normal-type-a").removeClass("active normal-type-b");
        }
    });

    $select_mode.change(function() {
        $(".viewport > img").attr("src","img/"+$(this).val()+"-"+$select_type.val()+".png");
        $(".sidebar").removeClass("normal hyper");
        $(".sidebar").addClass($(this).val());
        if($(this).val() == "normal") {
            $(".viewport-normal-type-a").addClass("active");
            if($select_type.val() == "type-b") {
                $(".viewport-normal-type-a").addClass("normal-type-b");
            } else {
                $(".viewport-normal-type-a").removeClass("normal-type-b");
            }
        } else {
            $(".viewport-normal-type-a").removeClass("active normal-type-b");
        }



    })

    $("#click_fileUpload").click(function() {
        $("#fileUpload").click();
        $('input[type=file]').change(function () {
            //console.dir(this.files[0].name)
            $("#fileUpload__name").text(this.files[0].name);
        })
    })

    $(".has-outline").click(function() {
        $(this).addClass("outline-active");
    })

    $(document).mouseup(function (e)
    {
        var container = $(".has-outline")

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.removeClass("outline-active");
        }
    });

//    ==============MIN RANGE NUMBER============

    $('.text-input-number').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('.text-input-number').keyup(function (event) {
        var min = $(this).attr('data-min');
        var max = $(this).attr('data-max');
        var val = parseInt($(this).val(), 10);

        //alert('up val::::' + val);
        //console.log('up val::::' + val);

        if (val < min) {
            $(this).val(min);
            //return false;
        }

        if (val > max) {
            $(this).val(max);
            //return false;
        }

        //console.log('::::::::::::::::::::::::::::::::');
        //return true;
    });

    $('.text-input-number').keypress(function (event) {
        var min = $(this).attr('data-min');
        var max = $(this).attr('data-max');
        var val = parseInt($(this).val(), 10);

        //alert('press val::::' + val);
        //console.log('press val::::' + val);

        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        //var final = val + String.fromCharCode(charCode);
        //if (final < min || final > max)
        // return false;

        return true;

    });

    $('.text-input-number').keydown(function (event) {
        var min = $(this).attr('data-min');
        var max = $(this).attr('data-max');
        var val = parseInt($(this).val(), 10);

        //alert('down val::::' + val);
        //console.log('down val::::' + val);

        if (!isNaN(val)) {
            if (event.keyCode == 37) {
                //alert('37');
            }
            else if (event.keyCode == 38) {
                //Up
                if (val > max-1)
                    return false;
            }
            else if (event.keyCode == 39) {
                //alert('39');
            }
            else if (event.keyCode == 40) {
                //Down
                if (val - 1 < min)
                    return false;
            }
        }
        return true;

    });
//    ===============end MIN RANGE NUMBER==============


//    ================================================
//    effect layer number

    $("#inner-layer-number").keyup(function() {
        var get_this_number = $(this).val();
        $(".viewport-normal-type-a .block__hidden").each(function(index, element) {
            if(index == get_this_number -1) {
                $(this).addClass("active");
                $(".viewport-normal-type-a .block__hidden").not($(this)).removeClass("active");
                var get_current_value = $(this).find(".block__number").attr("data-number");
                $("#slider-neurons-txt").val(get_current_value);
                $("#slider-neurons").slider("value" , get_current_value);
            }
       })
    });

    $("#txt-score-1").keyup(function() {
        var get_this_number = $(this).val();
        $("#score-1").slider("value" , get_this_number);
        setting_number_layers();
    });

    $("#slider-neurons-txt").keyup(function() {
        $("#slider-neurons").slider("value" , getFakeValue($(this).val()));
        setting_cube();
        setting_height_line();
    });

    $(".block__hidden__wrap").on("click", ".block__hidden", function () {
        var get_current_index = $('.block__hidden').index(this)+1;
        $("#inner-layer-number").val(get_current_index);
        $("#slider-neurons-txt").val($(this).find(".number").text());
        $("#slider-neurons").slider("value" , getFakeValue($(this).find(".number").text()));
        $(this).addClass("active");
        $(".viewport-normal-type-a .block__hidden").not($(this)).removeClass("active");
        $(this).find(".cube").effect( "shake", { direction: "up", times: 1, distance: 3} );
    })

    var get_value = parseInt($("#txt-score-1").val());
    $(".block__hidden__wrap").width((164 * get_value));
    $(".block__hidden__wrap .block__hidden").last().addClass("last");

//    ================================================

});

function setting_cube() {
    $(".block__hidden.active .block__number").find(".cube").remove();
    var slider_nerous_val = $("#slider-neurons-txt").val();
    var number_cube = 0;
    $(".block__hidden.active .block__number").attr("data-number",slider_nerous_val);
    $(".block__hidden.active .number").text(slider_nerous_val);
    if(slider_nerous_val >= 1 && slider_nerous_val <= 9) {
        for(var i=0; i< slider_nerous_val; i++) {
            $("<div class='cube'><img src='img/cube-01.png' /> </div>").clone().appendTo($(".block__hidden.active .block__number"))
        }
    } else if(slider_nerous_val >= 10 && slider_nerous_val <= 99) {
        number_cube = parseInt(slider_nerous_val / 10);
        for(var c2=0; c2< number_cube; c2++) {
            $("<div class='cube'><img src='img/cube-02.png' /> </div>").clone().appendTo($(".block__hidden.active .block__number"))
        }
    } else if(slider_nerous_val >= 100 && slider_nerous_val <= 999) {
        number_cube = parseInt(slider_nerous_val / 100);
        for(var c3=0; c3< number_cube; c3++) {
            $("<div class='cube'><img src='img/cube-03.png' /> </div>").clone().appendTo($(".block__hidden.active .block__number"))
        }
    } else if(slider_nerous_val >= 1000 && slider_nerous_val <= 9999) {
        number_cube = parseInt(slider_nerous_val / 1000);
        for(var c4=0; c4< number_cube; c4++) {
            $("<div class='cube'><img src='img/cube-04.png' /> </div>").clone().appendTo($(".block__hidden.active .block__number"))
        }
    }
    else if(slider_nerous_val >= 10000 && slider_nerous_val <= 100000) {
        number_cube = parseInt(slider_nerous_val / 10000);
        for(var c5=0; c5< number_cube; c5++) {
            $("<div class='cube'><img src='img/cube-04.png' /> </div>").clone().appendTo($(".block__hidden.active .block__number"))
        }
    }
}

function setting_height_line() {
    var maximum_height = null;
    $(".block__hidden").each(function() {
        var value = $(this).height();
        maximum_height = (value > maximum_height) ? value : maximum_height;
    })


    $(".block__hidden").each(function () {
        var line_height = maximum_height - 62 - $(this).find(".block__number").height() + 40;
        $(this).find(".line").height(line_height);
        $(this).find(".line").css("bottom",-line_height - 10 + 'px');
    })
}

function setting_number_layers() {

    var get_value = parseInt($("#txt-score-1").val());
    var get_current_value = $(".block__hidden__wrap .block__hidden").length;
    $(".block__hidden__wrap .block__hidden").removeClass("last");

    if(get_current_value < get_value) {
        for(var i=get_current_value+1; i<= get_value; i++) {
                $(".block__hidden.hidden__clone").find(".inner-number").text(i);
                $(".block__hidden.hidden__clone").clone().appendTo($(".block__hidden__wrap"));
                $(".block__hidden__wrap").find(".block__hidden").removeClass("hidden__clone");
        }
    } else {
        var counting = 1;
        for(var i2=get_value; i2< get_current_value; i2++) {
           $(".block__hidden__wrap .block__hidden").eq(get_current_value-counting).remove();

           if($(".block__hidden__wrap .block__hidden").eq(get_current_value-counting).hasClass("active")) {
               $(".block__hidden__wrap .block__hidden").first().click();
           }
           counting++;
        }
    }

    if (get_value<=3){
        $(".viewport-normal-type-a").removeClass("value-1 value-2 value-3");
        $(".line--bottom-02.normal-type-b-active").removeClass("value-1 value-2 value-3");
        $(".viewport-normal-type-a").addClass("value-"+ get_value);
        $(".line--bottom-02.normal-type-b-active").addClass("value-"+ get_value);
    }else if(!isNaN(get_value)){
        $(".viewport-normal-type-a").removeClass("value-1 value-2 value-3");
        $(".line--bottom-02.normal-type-b-active").removeClass("value-1 value-2 value-3");
    }


    $(".block__hidden__wrap .block__hidden").last().addClass("last");

    $(".block__hidden__wrap").width((164 * get_value));
    $(".viewport-content").width((164 * get_value) + 243);

    setting_height_line();
}

