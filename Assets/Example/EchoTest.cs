﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class EchoTest : MonoBehaviour {
    IEnumerator Start () {
        Debug.Log("Run");
		WebSocket w = new WebSocket(new Uri("ws://dev.gumiviet.com:8000/ws"));
		yield return StartCoroutine(w.Connect());
		//w.SendString("Hi there");
		int i=0;
		while (true)
		{
			string reply = w.RecvString();
			if (reply != null)
			{
				Debug.Log ("Received: "+reply);	
			}
			if (w.error != null)
			{
				Debug.LogError ("Error: "+w.error);
				break;
			}
			yield return 0;
		}
		w.Close();
	}
}
