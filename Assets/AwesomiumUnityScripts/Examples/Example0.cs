using UnityEngine;
using System.Collections;
using LitJson;


[RequireComponent(typeof(AwesomiumUnityWebTexture))]
public class Example0 : MonoBehaviour {
    
    private AwesomiumUnityWebTexture m_WebTexture = null;
    AwesomiumUnityWebCore m_WebCore;

    // This example page already contains some buttons that will call our functions.
    // You can view the page source freely on GitHub at https://github.com/Rycul/AwesomiumUnity/blob/master/AwesomiumUnityScripts/Examples/Example0/index.php.
    public string m_URL = "https://github.com/Rycul/AwesomiumUnity/blob/master/AwesomiumUnityScripts/Examples/Example0/index.php";

    public GameObject m_Prefab = null;

    [SerializeField]
    private GameObject chartPanel;
    [SerializeField]
    private GameObject colorPanel;
    string data;

    // Use this for initialization
    void Start() {
        // Obtain the web texture component.
        m_WebTexture = GetComponent<AwesomiumUnityWebTexture>();
       
      

        // Check to make sure we have an instance.
        if (m_WebTexture == null) {
            DestroyImmediate(this);
        }

        m_WebTexture.Initialize();

        // Bind some C# functions to javascript functions.

        m_WebTexture.WebView.BindJavaScriptCallback("display3dChart", this.Callback_3dView);

        
        
        

        m_WebTexture.WebView.FinishLoadingFrame += myFinishLoadingCallback;
        m_WebTexture.WebView.ChangeAddressBar += myChangeAddressBarCallback;
        m_WebTexture.WebView.ShowCreatedWebView += myShowCreatedWebView;
       
       

        m_WebTexture.LoadURL(m_URL);




        StartCoroutine("ConnectAndGetDataSocket");
    }

    IEnumerator ConnectAndGetDataSocket() {
        WebSocket w = new WebSocket(new System.Uri("ws://112.213.91.164:8000/ws"));
        yield return StartCoroutine(w.Connect());
        //w.SendString("Hi there");
        while (true) {
            string reply = w.RecvString();
            int i = 0;
            if (reply != null) {
               
                JsonData jsonData = JsonMapper.ToObject(reply);
                if (jsonData.Count >= 0 && jsonData["mode"].ToString().Equals("sample_data_get")) {
                    data = reply;
                    //Debug.Log("Received: " + data);
                }
            }
            if (w.error != null) {
                Debug.LogError("Error: " + w.error);
                break;
            }
            yield return 0;
        }
        w.Close();
    }

    void Callback_3dView( AwesomiumUnityWebView _Caller ) {
        //Debug.Log("CLICKED Callback_3dView!");

        if(data != null) {
            chartPanel.SetActive(true);
            colorPanel.SetActive(true);
            chartPanel.GetComponent<DrawChartPopup>().JsonToObject(data);
        }
        //GameObject ChartPanel = Instantiate(Resources.Load("3DView") as GameObject);
        //ChartPanel.transform.localPosition = new Vector3(0, 2, 0);
        //ChartPanel.transform.localScale = new Vector3(1.9f, 1, 1.6f);
    }

    void myFinishLoadingCallback( AwesomiumUnityWebView _Caller, string url, System.Int64 frameid, bool ismainframe ) {
        Debug.Log("Finished loading URL: " + url + ", frameid: " + frameid + ", ismainframe: " + ismainframe);
    }

    void myChangeAddressBarCallback( AwesomiumUnityWebView _Caller, string url ) {
        Debug.Log("Changed url to: " + url);
    }

    void myShowCreatedWebView( AwesomiumUnityWebView _Caller, AwesomiumUnityWebView _NewView, string _OpenerURL, string _TargetURL, bool _IsPopUp ) {
        Debug.Log("Created new web view from '" + _OpenerURL + "' to target '" + _TargetURL + "' (is popup: " + _IsPopUp + ").");

        if (m_Prefab != null) {
            GameObject go = GameObject.Instantiate(m_Prefab) as GameObject;
            AwesomiumUnityWebTexture webtexture = go.GetComponent<AwesomiumUnityWebTexture>();
            webtexture.m_Width = 1920;
            webtexture.m_Height = 1080;
            webtexture.WebView = _NewView;
            webtexture.Initialize();
        }
    }

    void JSStringResultCallback( AwesomiumUnityWebView _Caller, string _String ) {
        Debug.Log("JS Result: " + _String);
    }

    void Update() {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 50)) {
            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.name == "btnClose") {       
                Application.Quit();
            }
        }
        //if (Input.GetKeyUp(KeyCode.M)) {
        //    AwesomiumUnityWebView.JavaScriptExecutionCallbacks callbacks = new AwesomiumUnityWebView.JavaScriptExecutionCallbacks();
        //    callbacks.StringResult = JSStringResultCallback;
        //    m_WebTexture.WebView.ExecuteJavaScriptWithResult("document.title", callbacks);

            //}

            //if (Input.GetKeyUp(KeyCode.S)) {
            //    m_WebTexture.WebView.ExecuteJavaScript("alert(\"test\");", null);
            //}
    }
}
