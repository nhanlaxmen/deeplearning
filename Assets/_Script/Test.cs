﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    private Transform target;
    public float minX, maxX;
    public float minZ, maxZ;
    //-2.5 2.5 30 30
    void Start() {
        target = this.transform;
    }

    void Update() {
        Vector3 viewPos = Camera.main.WorldToViewportPoint(target.position);

        if ((minX <= viewPos.x && viewPos.x <= maxX) && (minZ <= viewPos.z && viewPos.z <= maxZ) && (Camera.main.transform.position.y > this.transform.position.y)) {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        } else if ((minX > viewPos.x)) {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        } else if ((viewPos.x > maxX)) {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        } else if ((minZ > viewPos.z)) {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        } else if ((viewPos.z > maxZ)) {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        } else
            gameObject.GetComponent<MeshRenderer>().enabled = false;
    }

    void OnTriggerEnter( Collider other ) {
        Debug.Log("Enter:" + other.name);
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    void OnTriggerExit( Collider other ) {
        Debug.Log("Exit" + other.name);
        gameObject.GetComponent<MeshRenderer>().enabled = false;

    }


}

