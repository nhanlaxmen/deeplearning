﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlChart : MonoBehaviour {
    [SerializeField]
    Transform XYZ;
    float rotSpeed = 50f;
    float speed = 5.0f;
    Vector3 speedZoom;
    float screenHeight;
    float screenWidth;
    float objectWidth;
    float objectHeight;
    Vector3 toPos;
    float vectorY = 0;
    bool isZoomIn = false;
    bool isZoomOut = false;
    void Start() {
        screenHeight = Screen.height;
        screenWidth = Screen.width;
        objectWidth = 10f;
        objectHeight = objectWidth * 9 / 16;
        speedZoom = new Vector3(0, 0, 0);
        vectorY = transform.position.y;
    }

    void Update() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButton(0)) {
            startRotation();
        }
        if (Physics.Raycast(ray, out hit, 50)) {
            //if (Input.GetAxis("Mouse ScrollWheel") > 0 && transform.localPosition.y > 3) {
            //    // ZOOM OUT
            //    getPositionZoomOut(hit.point);
            //}
            //if (Input.GetAxis("Mouse ScrollWheel") < 0 && transform.localPosition.y < 15) {
            //    // ZOOM IN
            //    getPositionZoomIn(hit.point);
            //}
            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.name == "Node(Clone)") {
                Vector3 cubePos = hit.collider.transform.position;
                gameObject.transform.position = gameObject.transform.position + new Vector3(-cubePos.x, +2.5f, -cubePos.z);
            }

            //if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.name == "btnBack") {
            //    //Debug.Log("click close button");
            //    //transform.parent.gameObject.SetActive(false);
            //    GetComponentInParent<DrawChartPopup>().Close3dChart();
            //}

            if (Input.GetAxis("Mouse ScrollWheel") > 0 && transform.position.y > 8) {
                // ZOOM OUT
                //getPositionZoomOut(hit.point);
                float newPosX = transform.position.x + ((screenWidth / 2 - Input.mousePosition.x) / (screenWidth) * objectWidth) * (14 - vectorY) / 14;
                float newPosZ = transform.position.z + ((screenHeight / 2 - Input.mousePosition.y) / (screenHeight) * objectHeight) * (14 - vectorY) / 14;
                float newPosY = transform.position.y - 1.5f;
                toPos = new Vector3(newPosX, newPosY, newPosZ);
                isZoomOut = true;
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0) {
                // ZOOM IN
                getPositionZoomIn(hit.point);
            }
        } else {
            if (Input.GetAxis("Mouse ScrollWheel") > 0 && transform.position.y > 1) {
                float newPosX = transform.position.x + ((screenWidth / 2 - Input.mousePosition.x) / (screenWidth) * objectWidth) * (14 - vectorY) / 14;
                float newPosZ = transform.position.z + ((screenHeight / 2 - Input.mousePosition.y) / (screenHeight) * objectHeight) * (14 - vectorY) / 14;
                float newPosY = transform.position.y - 1.5f;
                toPos = new Vector3(newPosX, newPosY, newPosZ);
                isZoomOut = true;
            } else if (Input.GetAxis("Mouse ScrollWheel") < 0 && transform.position.y < 100) {
                float newPosX = transform.position.x + ((Input.mousePosition.x - screenWidth / 2) / (screenWidth) * objectWidth) * (14 - vectorY) / 14;
                float newPosZ = transform.position.z + ((Input.mousePosition.y - screenHeight / 2) / (screenHeight) * objectHeight) * (14 - vectorY) / 14;
                float newPosY = transform.position.y + 1.5f;
                toPos = new Vector3(newPosX, newPosY, newPosZ);
                isZoomIn = true;
            }
        }
        if (this.isZoomOut || this.isZoomIn) {
            startZoom();
        }
    }

    void startRotation() {
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad * 0.1f;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad * 0.1f;
        transform.RotateAround(Vector3.up * 0.01f, -rotX);
        transform.RotateAround(Vector3.left * 0.01f, rotY);
        XYZ.RotateAround(Vector3.up * 0.01f, -rotX);
        XYZ.RotateAround(Vector3.left * 0.01f, rotY);
    }

    void startZoom() {
        if (this.isZoomIn) {
            if (speedZoom == new Vector3(0, 0, 0)) {
                speedZoom = (toPos - transform.position) * 1.5f;
            }
            transform.position = transform.position + speedZoom * Time.deltaTime;
            if (transform.position.y >= toPos.y) {
                this.isZoomIn = false;
                speedZoom = new Vector3(0, 0, 0);
                vectorY += 1.5f;
            }
        } else if (this.isZoomOut) {
            if (speedZoom == new Vector3(0, 0, 0)) {
                speedZoom = (toPos - transform.position) * 1.5f;
            }
            transform.position = transform.position + speedZoom ;
            //transform.position = transform.position + speedZoom * Time.deltaTime;
            if (transform.position.y <= toPos.y) {
                this.isZoomOut = false;
                speedZoom = new Vector3(0, 0, 0);
                vectorY -= 1.5f;
            }
        }
    }

    void getPositionZoomOut( Vector3 point ) {
        Vector3 mousePos = Input.mousePosition;
        vectorY = point.y;
        if (transform.position.y > 1) {
            toPos = gameObject.transform.position + new Vector3(point.x*0.001f, 0 , point.z*0.001f);
            isZoomOut = true;
        }
    }

    void getPositionZoomIn( Vector3 point ) {
        Vector3 mousePos = Input.mousePosition;
        vectorY = point.y;
        if (transform.position.y < 100) {
            toPos = gameObject.transform.position + new Vector3(-point.x, +1.5f, -point.z);
            isZoomIn = true;
        }
    }
}

