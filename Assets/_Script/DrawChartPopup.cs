﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using LitJson;


public class DrawChartPopup : MonoBehaviour {

    [SerializeField]
    private int numberNode;

    [SerializeField]
    private Transform Control;
    [SerializeField]
    private Rect rect;
    [SerializeField]
    private Texture backBtn;


    public Transform colorPanel;

    private float countTime;
    private int maxTime;

    private int[] numberLabel = new int[10];

    void Awake() {
        //colorPanel = GameObject.Find("Canvas").transform.GetChild(0).transform;
    }

    void Start() {
        //colorPanel.gameObject.SetActive(true);
        maxTime = UnityEngine.Random.Range(5, 10);
        //InvokeRepeating("LoadNode", 0f, 0.02f);
        //StartCoroutine("ConnectAndGetDataSocket");


    }

    // public void GetData() {
    //     StartCoroutine("ConnectAndGetDataSocket");
    // }

    //IEnumerator ConnectAndGetDataSocket() {
    //     Debug.Log("Run");
    //     WebSocket w = new WebSocket(new Uri("ws://192.168.1.28:8000/ws"));
    //     yield return StartCoroutine(w.Connect());
    //     w.SendString("Hi there");
    //     int i = 0;
    //     while (true) {
    //         string reply = w.RecvString();
    //         if (reply != null) {
    //             Debug.Log("Received: " + reply);
    //             JsonToObject(reply);
    //         }
    //         if (w.error != null) {
    //             Debug.LogError("Error: " + w.error);
    //             break;
    //         }
    //         yield return 0;
    //     }
    //     w.Close();
    // }


    public void JsonToObject( string data ) {

        JsonData jsonData = JsonMapper.ToObject(data);

        if (jsonData.Count >= 0 && jsonData["mode"].ToString().Equals("sample_data_get")) {
            int nodeNumber = jsonData["data"].Count;
            //Debug.Log(jsonData["mode"].ToString());
            for (int i = 0; i < nodeNumber; i++) {
                string imgURl = jsonData["data"][i]["img"].ToString();
                float x = float.Parse(jsonData["data"][i]["axis"][0].ToString());
                float y = float.Parse(jsonData["data"][i]["axis"][1].ToString());
                float z = float.Parse(jsonData["data"][i]["axis"][2].ToString());
                int colorLabel = int.Parse(jsonData["data"][i]["label"].ToString());
                LoadNode(imgURl, x, y, z, colorLabel);
            }

        }
    }

    public void Close3dChart() {
        foreach(Transform child in transform.GetChild(0).transform) {
            Destroy(child.gameObject);
        }
        colorPanel.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);

    }

    void LoadNode( string imgUrl, float x, float y, float z, int colorLabel ) {
        //Debug.Log(imgUrl + " " + x.ToString() + " " + y.ToString() + " " + z.ToString() + " " + colorLabel.ToString());

        GameObject child = Instantiate(Resources.Load("Node") as GameObject);
        child.transform.SetParent(Control);
        child.transform.localPosition = new Vector3(x, y, z);
        numberLabel[colorLabel]++;
        if (numberLabel[colorLabel] >= 10)
            colorPanel.GetChild(colorLabel + 1).GetComponent<Text>().text = "   *   " + colorLabel.ToString() + "            " + numberLabel[colorLabel].ToString();
        else
            colorPanel.GetChild(colorLabel + 1).GetComponent<Text>().text = "   *   " + colorLabel.ToString() + "              " + numberLabel[colorLabel].ToString();
        StartCoroutine(downLoadUImageFromURL(imgUrl, child));

    }



    IEnumerator downLoadUImageFromURL( string imgUrl, GameObject cube ) {
        // Start a download of the given URL
        WWW www = new WWW("http://112.213.91.164:8000" + imgUrl);

        // Wait for download to complete
        yield return www;

        // assign texture
        cube.GetComponent<MeshRenderer>().material.mainTexture = www.texture;

    }

}
