﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionChart : MonoBehaviour {
    [SerializeField]
    Transform XYZ;
    float rotSpeed = 50f;
    void Update() {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Vector3 pos = new Vector3();
        if (Physics.Raycast(ray, out hit, 100)) {

            if (hit.collider.gameObject.name == "Node(Clone)") {
                Vector3 cubePos = hit.collider.transform.position;
                pos = cubePos;   
            }
            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.name == "btnBack") {
                
                GetComponentInParent<DrawChartPopup>().Close3dChart();
            }

            if (Input.GetAxis("Mouse ScrollWheel") > 0 && Camera.main.orthographicSize > 0.3f) {
                Camera.main.orthographicSize-= 0.1f;

                float step = 10f * Time.deltaTime;

                Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(pos.x, 49.9f , pos.z), step);
               
               
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0 && Camera.main.orthographicSize < 5.32f) {
                Camera.main.orthographicSize+= 0.1f;
                float step = 1f * Time.deltaTime;
                Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(0, 49.9f, 0), step);
            }
        }

        if (Input.GetMouseButton(0)) {
            startRotation();
        }
    }
    void startRotation() {
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad * 0.1f;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad * 0.1f;
        transform.RotateAround(Vector3.up * 0.01f, -rotX);
        transform.RotateAround(Vector3.left * 0.01f, rotY);
        XYZ.RotateAround(Vector3.up * 0.01f, -rotX);
        XYZ.RotateAround(Vector3.left * 0.01f, rotY);
    }





}

